<?php
namespace AutoForm\Input;

/**
 * Select input
 *
 * @author ricardo <srsilveira@gmail.com>
 */
class Select extends \AutoForm\Builder\SelectBuilder {
    //block text type
    public function setType($type) {
        return parent::setType('select');
    }
}