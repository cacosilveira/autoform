<?php
namespace AutoForm\Input;

/**
 * Form checkbox field
 *
 * @author ricardo <srsilveira@gmail.com>
 */
class Checkbox extends \AutoForm\Builder\CheckboxBuilder {

}
