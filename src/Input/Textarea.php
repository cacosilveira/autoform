<?php
namespace AutoForm\Input;
/**
 * Textarea do form
 *
 * @author ricardo <srsilveira@gmail.com>
 */
class Textarea extends \AutoForm\Builder\TextareaBuilder {
    public function __construct() {
        $this->setType('textarea');
    }
}
