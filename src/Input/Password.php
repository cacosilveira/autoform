<?php
namespace AutoForm\Input;

/**
 * Form password field
 *
 * @author ricardo <srsilveira@gmail.com>
 */
class Password extends \AutoForm\Builder\InputBuilder {
    public function __construct(){
        $this->setType('password');
    }
}
