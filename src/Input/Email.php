<?php
namespace AutoForm\Input;

/**
 * Form text field
 *
 * @author ricardo <srsilveira@gmail.com>
 */
class Email extends \AutoForm\Builder\InputBuilder {
    public function __construct(){
        $this->setType('email');
    }
}
