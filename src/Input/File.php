<?php
namespace AutoForm\Input;

/**
 * Select input
 *
 * @author ricardo <srsilveira@gmail.com>
 */
class File extends \AutoForm\Builder\FileBuilder {
    //block text type
    public function setType($type) {
        return parent::setType('file');
    }
}