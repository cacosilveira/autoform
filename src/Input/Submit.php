<?php
namespace AutoForm\Input;

/**
 * Form submit field
 *
 * @author ricardo <srsilveira@gmail.com>
 */
class Submit extends \AutoForm\Builder\InputBuilder {
    public function __construct(){
        $this->setType('submit');
    }
}
