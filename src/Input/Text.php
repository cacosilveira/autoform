<?php
namespace AutoForm\Input;

/**
 * Form text field
 *
 * @author ricardo <srsilveira@gmail.com>
 */
class Text extends \AutoForm\Builder\InputBuilder {

    //block text type
    public function setType($type) {
        return parent::setType('text');
    }
}
