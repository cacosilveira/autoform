<?php

/**
 * Check if everything is good
 *
 * @author ricardo <srsilveira@gmail.com>
 */
namespace AutoForm\Evaluator;
class FormEvaluator {
    protected $form;
    public function __construct(\AutoForm\Builder\FormBuilder $form) {
        $this->form = $form;
        $this->evaluate();
    }
    
    /**
     * Evaluates all form methods
     * @todo Separate all methods
     * @throws \Exception
     */
    protected function evaluate(){
        
        $this->evaluateMethod();
        $this->evaluateId();
        
        return true;
    }
    
    protected function evaluateMethod(){
        //post or get for the form
        if(!in_array($this->form->getMethod(), ['get','post'])){
            throw new \Exception('Form method must be "get" or "post"');
        }
    }
    
    protected function evaluateId(){
        //we really need a ID
        if(strlen($this->form->getId())==0){
            throw new \Exception('Missing Form id');
        }
        if(strpos($this->form->getId(), ' ')>0){
            throw new \Exception('No spaces on id');
        }
    }
    
}
