<?php

/**
 * The form builder
 *
 * @author ricardo <srsilveira@gmail.com>
 * 
 */

namespace AutoForm\Builder;

class FormBuilder {

    private $action,
            $method,
            $enctype,
            $id,
            $class,
            $name,
            $fields;

    function getAction() {
        return $this->action;
    }

    function getMethod() {
        return $this->method;
    }

    function getEnctype() {
        return $this->enctype;
    }

    function getId() {
        return $this->id;
    }

    function getClass() {
        return $this->class;
    }

    function getName() {
        return $this->name;
    }

    public function setAction($action) {
        $this->action = $action;
        return $this;
    }

    /**
     * The form method. You can use: "get" and "post"
     * @param string $enctype
     * @return $this
     */
    public function setMethod($method) {
        $this->method = $method;
        return $this;
    }

    /**
     * The form's Enctype. You can use: "application/x-www-form-urlencoded", "multipart/form-data" e "text/plain"
     * Use multipart/form-data for upload.
     * @param string $enctype
     * @return $this
     */
    public function setEnctype($enctype) {
        $this->enctype = $enctype;
        return $this;
    }

    /**
     * The form's ID do not use spaces
     * @param string $id
     * @return $this
     */
    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    /**
     * The form's CSS class
     * @param string $class
     * @return $this
     */
    public function setClass($class) {
        $this->class = $class;
        return $this;
    }

    /**
     * The form's name.
     * @param string $name
     * @return $this
     */
    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    /**
     * We build our form
     * @todo fields! we need fields
     * @return string
     */
    public function build() {
        new \AutoForm\Evaluator\FormEvaluator($this);
        $build = $this->start();
        foreach ($this->fields as $key=>$val) {
            $build .= '<div class="form-group">';
            $build .= $val->build();
            $build .= '</div>';
        }

        $build .= $this->end();
        return $build;
    }
    
    /**
     * Get the <form.. html
     * @return string
     */
    public function start(){
        return '<form '
                . 'name="' . $this->getName() . '" '
                . 'action="' . $this->getAction() . '" '
                . 'method="' . $this->getMethod() . '" '
                . 'enctype="' . $this->getEnctype() . '" '
                . 'class="' . $this->getClass() . '" id="' . $this->getId() . '">';
    }
    
    /**
     * ends the html form tag
     * @return string
     */
    public function end(){
        return '</form>';
    }
    
    /**
     * Give the field id and it will return the html
     * @param string $fieldId
     * @return Object
     */
    public function getField($fieldId){
        $build  = '<div class="form-group">';
        $build .= $this->fields[$fieldId]->build();//$val->build();
        $build .= '</div>';
        return $build;
    }

    /*
     * FIELDS
     */

    /**
     * Easy way to add the fields
     * @param array $field
     */
    public function add(array $field) {
        
        if(strlen($field['id'])==0){
            throw new Exception('You need the id field');
        }
        
        switch ($field['type']) {
            /*
             * Text
             */
            case 'text':
                $this->addInputText(
                    (new \AutoForm\Input\Text())
                        ->setId($field['id'])
                        ->setClass($field['class'])
                        ->setName($field['name'])
                        ->setValue($field['value'])
                        ->setLabelName($field['labelName'])
                        ->setHelpText($field['helpText'])
                        ->setPlaceholder($field['placeholder'])
                        ->setRequired($field['required'])
                        ->setType('text')
                        
                );
                break;
            
            /*
             * Email 
             */
            case 'email':
                $this->addInputEmail(
                    (new \AutoForm\Input\Email())
                        ->setId($field['id'])
                        ->setClass($field['class'])
                        ->setName($field['name'])
                        ->setValue($field['value'])
                        ->setLabelName($field['labelName'])
                        ->setHelpText($field['helpText'])
                        ->setPlaceholder($field['placeholder'])
                        ->setRequired($field['required'])
                        ->setType('email')
                );
                break;
            
            /*
             * Password
             */
            case 'password':
                $this->addInputPassword(
                    (new \AutoForm\Input\Password())
                        ->setId($field['id'])
                        ->setClass($field['class'])
                        ->setName($field['name'])
                        ->setValue($field['value'])
                        ->setLabelName($field['labelName'])
                        ->setHelpText($field['helpText'])
                        ->setPlaceholder($field['placeholder'])
                        ->setRequired($field['required'])
                        ->setType('password')
                );
                break;

            /*
             * Submit
             */
            case 'submit':
                $this->addInputSubmit(
                    (new \AutoForm\Input\Submit())
                        ->setId($field['id'])
                        ->setClass($field['class'])
                        ->setName($field['name'])
                        ->setValue($field['value'])
                        ->setLabelName($field['labelName'])
                        ->setHelpText($field['helpText'])
                        ->setPlaceholder($field['placeholder'])
                        ->setRequired($field['required'])
                        ->setType('submit')
                );
                break;
            
            /*
             * Textarea
             */
            case 'textarea':
                $this->addInputTextarea(
                    (new \AutoForm\Input\Textarea())
                        ->setId($field['id'])
                        ->setClass($field['class'])
                        ->setName($field['name'])
                        ->setValue($field['value'])
                        ->setLabelName($field['labelName'])
                        ->setHelpText($field['helpText'])
                        ->setPlaceholder($field['placeholder'])
                        ->setRequired($field['required'])
                        ->setType('textarea')
                );
                break;
            
            /*
             * Checkbox
             */
            case 'checkbox':
                $this->addInputCheckbox(
                    (new \AutoForm\Input\Checkbox())
                        ->setId($field['id'])
                        ->setClass($field['class'])
                        ->setName($field['name'])
                        ->setValue($field['value'])
                        ->setLabelName($field['labelName'])
                        ->setHelpText($field['helpText'])
                        ->setPlaceholder($field['placeholder'])
                        ->setRequired($field['required'])
                        ->setChecked($field['checked'])
                        ->setType('text')
                );
                break;
            
            /*
             * Select
             */
            case 'select':
                $this->addInputSelect(
                    (new \AutoForm\Input\Select())
                        ->setId($field['id'])
                        ->setClass($field['class'])
                        ->setName($field['name'])
                        ->setValue($field['value'])
                        ->setLabelName($field['labelName'])
                        ->setHelpText($field['helpText'])
                        ->setPlaceholder($field['placeholder'])
                        ->setRequired($field['required'])
                        ->setOptions($field['options'])
                        ->setType('text')
                );
                break;
            
            /*
             * File
             */
            case 'file':
                $this->addInputFile(
                    (new \AutoForm\Input\File())
                        ->setId($field['id'])
                        ->setClass($field['class'])
                        ->setName($field['name'])
                        ->setValue($field['value'])
                        ->setLabelName($field['labelName'])
                        ->setHelpText($field['helpText'])
                        ->setPlaceholder($field['placeholder'])
                        ->setRequired($field['required'])
                        ->setType('file')
                );
                break;
            
            /*
             * If all fail we serve a generic text field
             */
            default:
                $this->addInputGeneric(
                    (new \AutoForm\Input\Generic())
                        ->setId($field['id'])
                        ->setClass($field['class'])
                        ->setName($field['name'])
                        ->setValue($field['value'])
                        ->setLabelName($field['labelName'])
                        ->setHelpText($field['helpText'])
                        ->setPlaceholder($field['placeholder'])
                        ->setRequired($field['required'])
                        ->setType($field['type'])
                );
                break;
        }
    }

    /**
     * Text field
     * @param \AutoForm\Input\Text $field
     */
    public function addInputText(\AutoForm\Input\Text $field) {
        $this->fields[$field->getId()] = $field;
        return $this;
    }

    /**
     * Email field
     * @param \AutoForm\Input\Email $field
     */
    public function addInputEmail(\AutoForm\Input\Email $field) {
        $this->fields[$field->getId()] = $field;
        return $this;
    }

    /**
     * Password field
     * @param \AutoForm\Input\Email $field
     */
    public function addInputPassword(\AutoForm\Input\Password $field) {
        $this->fields[$field->getId()] = $field;
        return $this;
    }

    /**
     * Submit field
     * @param \AutoForm\Input\Email $field
     */
    public function addInputSubmit(\AutoForm\Input\Submit $field) {
        $this->fields[$field->getId()] = $field;
        return $this;
    }

    /**
     * Generic field so we can have another type like number or phone
     * @param \AutoForm\Input\Email $field
     */
    public function addInputGeneric(\AutoForm\Input\Generic $field) {
        $this->fields[$field->getId()] = $field;
        return $this;
    }

    /**
     * Textarea form field
     * @param \AutoForm\Input\Textarea $textarea
     * @return $this
     */
    public function addInputTextarea(\AutoForm\Input\Textarea $field) {
        $this->fields[$field->getId()] = $field;
        return $this;
    }

    /**
     * Checkbox field
     * @param \AutoForm\Input\Checkbox $checkbox
     * @return $this
     */
    public function addInputCheckbox(\AutoForm\Input\Checkbox $field) {
        $this->fields[$field->getId()] = $field;
        return $this;
    }
    
    /**
     * select field
     * @param \AutoForm\Input\Select $select
     * @return $this
     */
    public function addInputSelect(\AutoForm\Input\Select $field) {
        $this->fields[$field->getId()] = $field;
        return $this;
    }
    
    /**
     * File
     * @param \AutoForm\Input\File $file
     * @return $this
     */
    public function addInputFile(\AutoForm\Input\File $field) {
        $this->fields[$field->getId()] = $field;
        return $this;
    }

}
