<?php
namespace AutoForm\Builder;

/**
 * Input Builder
 *
 * @author ricardo <srsilveira@gmail.com>
 */
class TextareaBuilder extends InputBuilder implements InputInterface {
    
    /**
     * @todo html decorator to use bootstrap, materializecss, etc
     * @return string
     */
    public function build(){
        $build = '<div class="form-group">'
                . '<label id="label_'.$this->getId().'" for="'.$this->getId().'">'.$this->getLabelName().'</label>'
                . '<textarea class="form-control '.$this->getClass().'" id="'.$this->getId().'" name="'.$this->getName().'">'.$this->getValue().'</textarea>'
                . '</div>';
        return $build;
    }


}
