<?php

/**
 * Description of InputTypeText
 *
 * @author ricardo <srsilveira@gmail.com>
 */

namespace AutoForm\Input\InputType;

interface InputTypeText {
    function setType();
    function setId();
    function setName();
    function setClass();
}
