<?php
namespace AutoForm\Builder;

/**
 * Interface for the Form Builder
 * All inputs must have those attributes
 * @author ricardo <srsilveira@gmail.com>
 */


interface InputInterface {
    function setType($type);
    function setId($id);
    function setName($name);
    function setClass($class);
    function setValue($value);
    function setLabelName($labelName);
    function setRequired($required);
    function setPlaceholder($placeholder);
    function setHelpText($helpText);
    
    function getType();
    function getId();
    function getName();
    function getClass();
    function getValue();
    function getLabelName();
    function getRequired();
    function getPlaceholder();
    function getHelpText();
}
