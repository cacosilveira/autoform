<?php
namespace AutoForm\Builder;

/**
 * Input Builder
 *
 * @author ricardo <srsilveira@gmail.com>
 */
class InputBuilder implements InputInterface {
    private $type,
            $id,
            $name,
            $class,
            $value,
            $labelName,
            $required,
            $placeholder,
            $helpText;
    
    public function getType() {
        return $this->type;
    }

    public function getId() {
        return $this->id;
    }

    public function getName() {
        return strlen($this->name)>0?$this->name:$this->getId();
    }

    public function getClass() {
        return $this->class;
    }

    public function getValue() {
        return $this->value;
    }

    public function getLabelName() {
        return $this->labelName;
    }
    
    public function getRequired() {
        return $this->required;
    }
    
    public function getPlaceholder() {
        return $this->placeholder;
    }
    
    public function getHelpText() {
        return $this->helpText;
    }
    
    

    public function setType($type) {
        $this->type = $type;
        return $this;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    public function setClass($class) {
        $this->class = $class;
        return $this;
    }

    public function setValue($value) {
        $this->value = $value;
        return $this;
    }

    public function setLabelName($labelName) {
        $this->labelName = $labelName;
        return $this;
    }
    
    public function setRequired($required) {
        $this->required = $required;
        return $this;
    }
    
    public function setPlaceholder($placeholder) {
        $this->placeholder = $placeholder;
        return $this;
    }
    
    public function setHelpText($helptext) {
        $this->helpText = $helptext;
        return $this;
    }

    /**
     * @todo html decorator to use bootstrap, materializecss, etc
     * @return string
     */
    public function build(){
        
        //there's help text?
        $helptext = (strlen($this->getHelpText())>0)?'<small id="emailHelp" class="form-text text-muted">'.$this->getHelpText().'</small>':'';
        
        //there's a placeholder?
        $placeholder = (strlen($this->getPlaceholder())>0)?'placeholder="'.$this->getPlaceholder().'"':'';
        
        $build = '<div class="form-group">'
                . '<label id="label_'.$this->getId().'" for="'.$this->getId().'">'.$this->getLabelName().'</label>'
                . '<input '.$placeholder.' class="form-control '.$this->getClass().'" type="'.$this->getType().'" id="'.$this->getId().'" name="'.$this->getName().'" value="'.$this->getValue().'">'
                . $helptext
                . '</div>';
        return $build;
    }


}
