<?php
namespace AutoForm\Builder;

/**
 * Description of SelectBuilder
 *
 * @author ricardo <srsilveira@gmail.com>
 */
class SelectBuilder extends InputBuilder implements InputInterface {
    
    private $options;
    
    /**
     * Get Select options
     * @return array
     */
    public function getOptions() {
        return $this->options;
    }

    /*
     * Set select options
     */
    public function setOptions(array $options) {
        $this->options = $options;
        return $this;
    }
    
    /**
     * @todo html decorator to use bootstrap, materializecss, etc
     * @return string
     */
    public function build(){
        
        $build = '<div class="form-group">
                <label id="label_'.$this->getId().'" for="'.$this->getId().'">'.$this->getLabelName().'</label>
                <select class="form-control '.$this->getClass().'" id="exampleFormControlSelect1">';
                 
        $build .= $this->buildOptions();
        
        $build .= '</select></div>';
        
        return $build;
    }
    
    protected function buildOptions(){
        $res = '';
        foreach ($this->options as $key => $val){
            $selected = ($key==$this->getValue())?'selected':'';
            $res .= '<option '.$selected.' value="'.$key.'">'.$val.'</option>';
        }
        return $res;
    }
}
