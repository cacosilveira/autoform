<?php
namespace AutoForm\Builder;

/**
 * Description of SelectBuilder
 *
 * @author ricardo <srsilveira@gmail.com>
 */
class FileBuilder extends InputBuilder implements InputInterface {
    
    /**
     * @todo html decorator to use bootstrap, materializecss, etc
     * @return string
     */
    public function build(){
        
        $build = '<div class="form-group">
                <label id="label_'.$this->getId().'" for="'.$this->getId().'">'.$this->getLabelName().'</label>
                <input type="file" class="form-control-file '.$this->getClass().'" id="'.$this->getId().'" name="'.$this->getName().'">';
        $build .= '</select></div>';
        
        return $build;
    }
    
}
