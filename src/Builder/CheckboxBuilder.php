<?php
namespace AutoForm\Builder;

/**
 * Checkbox Builder
 *
 * @author ricardo <srsilveira@gmail.com>
 */
class CheckboxBuilder extends InputBuilder implements InputInterface {
    private $checked;
    
    /**
     * 
     * @return bool
     */
    public function getChecked(){
        return  $this->checked;
    }
    
    /**
     * 
     * @param bool $checked
     * @return $this
     */
    public function setChecked(bool $checked) {
        $this->checked = $checked;
        return $this;
    }
    
    /**
     * @todo html decorator to use bootstrap, materializecss, etc
     * @return string
     */
    public function build(){
        $checked = $this->getChecked()===true?'checked':'';
        $build = '<div class="form-group form-check">'
                . '<input '.$checked.' type="checkbox" class="form-check-input '.$this->getClass().'" id="'.$this->getId().'" name="'.$this->getName().'" '.$this->getValue().'>'
                . '<label id="label_'.$this->getId().'" for="'.$this->getId().'">'.$this->getLabelName().'</label>'
                . '</div>';
        return $build;
    }


}
