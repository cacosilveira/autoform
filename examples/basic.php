<?php
ini_set('display_errors', 1);
error_reporting(E_ALL ^ E_STRICT ^ E_NOTICE ^ E_DEPRECATED ^ E_WARNING);

require '../vendor/autoload.php';

use AutoForm\Form;

//create the form
$form = new Form();

if(isset($_POST['send'])){
    var_dump($_POST,$_FILES);
}

//set the attributes
$form->setAction('')
    ->setClass('form')
    ->setId('autoform')
    ->setName('autoform')
    ->setMethod('post')
    ->setEnctype('multipart/form-data')
    ->setAction('');

/*
 * FORM FIELDS!
 */

//generic field, it gets the text layout
$form->add([
    'labelName'=>'Custom',
    'id'=>'custom',
    'type'=>'myothertype',
    'class'=>'',
    'name'=>'custom',
    'value'=>'',
    'placeholder'=>'I am a placeholder',
    'helpText' => '',
    'required'=>true
]);

//text field
$form->add([
    'labelName'=>'Nome',
    'id'=>'name',
    'type'=>'text',
    'class'=>'',
    'name'=>'name',
    'value'=>'Meu Nome',
    'helpText' => 'This is my name, there\'s many like it, but this one is mine.',
    'required'=>true
]);

//email
$form->add([
    'type'=>'email',
    'id'=>'email',
    'class'=>'',
    'name'=>'email',
    'value'=>'Meu@Email',
    'labelName'=>'Email'
]);


//password
$form->add([
    'type'=>'password',
    'id'=>'password',
    'class'=>'',
    'name'=>'password',
    'value'=>'a09s8d',
    'labelName'=>'Password'
]);

//number
$form->add([
    'type'=>'number',
    'id'=>'mygeneric',
    'class'=>'',
    'name'=>'mygeneric',
    'value'=>'99',
    'labelName'=>'Idade'
]);

//textarea
$form->add([
    'type'=>'textarea',
    'id'=>'obs',
    'class'=>'',
    'name'=>'obs',
    'value'=>'This is my text',
    'labelName'=>'Obs'
]);

//select with options
$form->add([
    'type'=>'select',
    'id'=>'sexo',
    'class'=>'',
    'name'=>'sexo',
    'value'=>'f',
    'options'=>[
        ''=>'Select',
        'm'=>'Masculino',
        'f'=>'Feminino'
    ],
    'labelName'=>'Sexo'
]);

//checkbox
$form->add([
    'type'=>'checkbox',
    'id'=>'checkme',
    'class'=>'',
    'name'=>'checkme',
    'value'=>'1',
    'checked'=>true,
    'labelName'=>'I did read and understand the terms'
]);

//file
$form->add([
    'type'=>'file',
    'id'=>'myfile',
    'class'=>'',
    'name'=>'myfile',
    'value'=>null,
    'labelName'=>'My File',
    'helpText' => 'Just DOC, DOCX and PDF allowed'
]);

//submit
$form->add([
    'type'=>'submit',
    'id'=>'send',
    'class'=>'btn btn-primary',
    'name'=>'send',
    'value'=>'Send'
]);

?>
<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

        <title>AutoForm</title>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <?php
                    /*
                     * Now we build our form
                     */
                    echo $form->build();
                    ?>
                </div>
            </div>
        </div>

        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    </body>
</html>