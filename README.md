# AutoForm

New developement at [beta](https://gitlab.com/cacosilveira/autoform/-/tree/beta)

A simple PHP form generador

Create forms quickly and easy with this generador. You just input a array with the parameter and the script gives you the html.

**Create the form with**
```
$form->setAction('')
    ->setClass('form')
    ->setId('autoform')
    ->setName('autoform')
    ->setMethod('post')
    ->setEnctype('multipart/form-data')
    ->setAction('');
```

**Insert each field with**
```
//text field
$form->add([
    'labelName'=>'Client Name',
    'id'=>'clientname',
    'type'=>'text',
    'class'=>'',
    'name'=>'clientname',
    'value'=>'',
    'placeholder'=>'John Doe',
    'helpText' => 'Insert full name here',
    'required'=>true
]);
```
The examples folder have all fields you can use.

**reate the form with**
```
echo $form->build();
```
